using System.Collections.Generic;

namespace OurLegacy.Location
{
    public struct Grid
    {
        public Point NorthEast { get; }
        public Point SouthEast { get; }
        public Point SouthWest { get; }
        public Point NorthWest { get; }

        public List<Point> InnerPoints { get; }

        public Grid(Point northEast, Point southEast, Point southWest, Point northWest, List<Point> innerPoints = null)
        {
            NorthEast = northEast;
            SouthEast = southEast;
            SouthWest = southWest;
            NorthWest = northWest;
            InnerPoints = innerPoints == null ? new List<Point>() : innerPoints;
        }

        public Grid UpdateBounds(Point northEast = null, Point southEast = null, Point southWest = null, Point northWest = null) =>
            new Grid(
                northEast: northEast != null ? NorthEast + northEast : NorthEast,
                southEast: southEast != null ? SouthEast + southEast : SouthEast,
                southWest: southWest != null ? SouthWest + southWest : SouthWest,
                northWest: northWest != null ? NorthWest + northWest : NorthWest
            );

        public Grid SetBounds(Point northEast, Point southEast, Point southWest, Point northWest) => new Grid(northEast, southEast, southWest, northWest);

        public Grid Move(Point offset) =>
            new Grid(
                northEast: NorthEast + offset,
                southEast: SouthEast + offset,
                southWest: SouthWest + offset,
                northWest: NorthWest + offset
            );
    }
}