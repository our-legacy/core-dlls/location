namespace OurLegacy.Location
{
    public struct Location
    {
        private Point Data { get; set; }

        public double X { get => Data.X; }
        public double Y { get => Data.Y; }
        public double Z { get => Data.Z; }

        public Location(Point data) { Data = data; }
        public Location(double x, double y, double z) { Data = new Point(x, y, z); }

        public Point Move(Point change) => Data = Data + change;
        public Point Move(double x, double y, double z) => Data = Data + new Point(x, y, z);
        public Point Set(Point change) => Data = change;
    }
}