namespace OurLegacy.Location
{
    public class Point
    {
        public double X { get; }
        public double Y { get; }
        public double Z { get; }

        public Point()
        {
            X = 0;
            Y = 0;
            Z = 0;
        }

        public Point(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Point operator +(Point a, Point b) => new Point(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        public static Point operator -(Point a, Point b) => new Point(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        public static Point operator *(Point a, Point b) => new Point(a.X * b.X, a.Y * b.Y, a.Z * b.Z);
        public static Point operator /(Point a, Point b) => new Point(a.X / b.X, a.Y / b.Y, a.Z / b.Z);
        public static Point operator %(Point a, Point b) => new Point(a.X % b.X, a.Y % b.Y, a.Z % b.Z);
        public static bool operator ==(Point a, Point b) => a.X == b.X && a.Y == b.Y && a.Z == b.Z;
        public static bool operator !=(Point a, Point b) => !(a == b);
        
        public override bool Equals(object obj)
        {
            var objAsPoint = obj as Point;
            return objAsPoint != null && objAsPoint == this;
        }

        public override int GetHashCode() => X.GetHashCode() ^ Y.GetHashCode() ^ Z.GetHashCode();
    }
}